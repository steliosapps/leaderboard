**#api endpoints**

**GetLeaderBoard** : gets all the leaderboard. Can limit the response to get top 10 for example

**GetPlayerById** : gets all the scores for player with specific id

**GetScoresByPlayerId** : gets all the scores for player with specific id

**AddPlayer** : adds new player info in db

**SubmitScore** : submits a players score. Needs specifc passphrase for the score to be accepted

**PurgeLeaderBoard** : purges scores. Needs specifc passphrase for the purge to be accepted


**#THIS PROJECT USES POSTGRES DATABASE**

**#migration scripts**

**#scripts to create database**

CREATE SEQUENCE IF NOT EXISTS playerIdSeq
    AS INT
    INCREMENT BY 1
    MINVALUE 1;

CREATE TABLE IF NOT EXISTS Player (
   playerId serial PRIMARY KEY,
   userName VARCHAR(200) UNIQUE NOT NULL,
   dateCreated DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS Score (
  playerId INT NOT NULL,
  score BIGINT NOT NULL,
  scoreDate DATE NOT NULL,
  FOREIGN KEY (playerId)
      REFERENCES Player (playerId)
);

**#scripts to reset database**

DROP SEQUENCE IF EXISTS playeridseq;

DROP TABLE IF EXISTS Score;

DROP TABLE IF EXISTS playerId;
